# README #

### What is this repository for? ###

Show dismissible errors or notifications in Bootstrap when using the Processwire CMS. 

### How do I get set up? ###

* Install the mod in the usual way. 
* Declare something like: 

$bs = new BootstrapFeedback;
echo $bs->success('It Works!');

Dependencies:
* Twitter bootstrap
* Processwire CMS